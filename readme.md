Basado en: https://docs.sonarqube.org/display/PLUG/Writing+Custom+Java+Rules+101

Reglas custom en src/main/java/org/sonar/samples/java/checks

Para agregar reglas al servidor de sonar, hacer mvn package y copiar el .jar en la carpeta extensions/plugins del servidor SonarQube.
Editar el Quality Profile para agregar las nuevas reglas.

Para agregar un proyecto a Sonar, hacer cd a la carpeta del proyecto y ejecutar el siguiente comando con una instancia de Sonar corriendo
> mvn sonar:sonar   -Dsonar.host.url=(URL del servidor de SonarQube)   -Dsonar.login=(token para hacer login en sonar)

(Asegurarse que el proyecto esté usando el Quality Profile que definimos)

