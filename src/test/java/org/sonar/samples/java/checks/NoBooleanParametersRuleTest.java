package org.sonar.samples.java.checks;
 
import org.junit.Test;
import org.sonar.java.checks.verifier.JavaCheckVerifier;
 
public class NoBooleanParametersRuleTest {

	@Test
	public void test() {
		JavaCheckVerifier.verify("src/test/files/NoBooleanParametersRule.java",
				new NoBooleanParametersRule());
	}
 
}