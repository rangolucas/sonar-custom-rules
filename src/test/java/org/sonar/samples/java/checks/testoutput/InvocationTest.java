package org.sonar.samples.java.checks.testoutput;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class InvocationTest {

	@Test
	public void testEquals() {
		Invocation inv = new Invocation("PrintStream", "println");
		Invocation sameInv = new Invocation("PrintStream", "println");
		Invocation otherInv = new Invocation("PrintStream", "print");
		Invocation anotherInv = new Invocation("Stream", "println");
		
		assertEquals(inv, sameInv);
		assertNotEquals(inv, otherInv);
		assertNotEquals(inv, anotherInv);
	}
}
