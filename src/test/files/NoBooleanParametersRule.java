class MyClass {
	private MyClass() {}
	
	public int method1() {return 1;}
	public int method2(boolean a) {return 0;} // Noncompliant
	public boolean method3(char a) {return true;}
	public void method2(boolean a, int b) {} // Noncompliant
}