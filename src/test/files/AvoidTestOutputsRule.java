import java.util.logging.Logger;

class MyClassTest {
	private MyClassTest() {}
	
	public int method1() {return 1;}
	public void method2() {System.out.println("hola");} // Noncompliant
	public void method3() {
		// Noncompliant@+1
		Logger logger = Logger.getLogger("Logger");
		logger.info("Un mensaje");
	} 
}