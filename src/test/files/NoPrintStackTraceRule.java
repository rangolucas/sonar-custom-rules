import java.lang.NullPointerException;

class MyClass {
	private MyClass() {}
	
	public int method1() {
		Object a = new Object();
		try {
			a.toString();
		}
		catch (NullPointerException e){}
		return 1;
	}
	
	public int method2(boolean a) {
		
		Exception e = new Exception();
		e.printStackTrace(); // Noncompliant
		return 0;
	}
	
	public boolean method3(char a) {return true;}
}