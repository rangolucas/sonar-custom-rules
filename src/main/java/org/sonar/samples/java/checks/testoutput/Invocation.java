package org.sonar.samples.java.checks.testoutput;

import org.sonar.plugins.java.api.semantic.Symbol;

public class Invocation {
	private String ownerName;
	private String methodName;
	
	public Invocation(String ownerName, String methodName) {
		this.ownerName = ownerName;
		this.methodName = methodName;
	}
	
	public Invocation(Symbol symbol) {
		this.ownerName = symbol.owner().toString();
		this.methodName = symbol.name();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
		result = prime * result + ((ownerName == null) ? 0 : ownerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invocation other = (Invocation) obj;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		if (ownerName == null) {
			if (other.ownerName != null)
				return false;
		} else if (!ownerName.equals(other.ownerName))
			return false;
		return true;
	}
	
	
	
	@Override
	public String toString() {
		return this.ownerName + "." + this.methodName;
	}
}
