package org.sonar.samples.java.checks;

import java.util.List;

import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.Tree.Kind;

import com.google.common.collect.ImmutableList;

@Rule(
		  key = "NoPrintStackTraceRule",
		  name = "Invocar al metodo printStackTrace",
		  description = "La invocación de printStackTrace() no define"
		  		+ " comportamiento suficiente para el manejo de una excepción.",
		  priority = Priority.MAJOR,
		  tags = {"codesmell"})
public class NoPrintStackTraceRule extends IssuableSubscriptionVisitor {

	@Override
	public List<Kind> nodesToVisit() {
		return ImmutableList.of(Kind.METHOD_INVOCATION);
	}

	@Override
	public void visitNode(Tree tree) {
		MethodInvocationTree method = (MethodInvocationTree) tree;
		Symbol symbol = method.symbol();
		if(symbol.name().equals("printStackTrace")){
			this.reportIssue(method, "No invocar printStackTrace");
		}
	}
}
