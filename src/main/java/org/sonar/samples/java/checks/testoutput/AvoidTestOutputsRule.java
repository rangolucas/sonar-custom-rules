package org.sonar.samples.java.checks.testoutput;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.Tree.Kind;

import com.google.common.collect.ImmutableList;

//TODO internacionalizacion


@Rule(
		  key = "AvoidTestOutputsRule",
		  name = "Output en clase test",
		  description = "Las clases de test no deberían generar outputs de "
		  		+ "ningún tipo.",
		  priority = Priority.MINOR,
		  tags = {"bad-practice"})
public class AvoidTestOutputsRule extends IssuableSubscriptionVisitor {
	
	@Override
	public List<Kind> nodesToVisit() {
		return ImmutableList.of(Kind.METHOD_INVOCATION);
	}
	
	@Override
	public void visitNode(Tree tree) {
		MethodInvocationTree methodInvocation = (MethodInvocationTree) tree;
		Symbol symbol = methodInvocation.symbol();
		
		if(generatesOutput(symbol)) {
			reportIssue(methodInvocation, "Las clases de tests no deberían"
					+ " generar outputs");
		}
	}
	
	private static boolean generatesOutput(Symbol symbol) {
		Set<Invocation> forbiddenInvocations = new HashSet<>();
		forbiddenInvocations.add(new Invocation("PrintStream", "println"));
		forbiddenInvocations.add(new Invocation("Logger", "getLogger"));
		
		Invocation invocation = new Invocation(symbol);
		return forbiddenInvocations.contains(invocation);
	}
}
